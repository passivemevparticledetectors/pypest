"""
ACHTUNG: ANGULAR ELECTRON SCATTERING IS BOLDLY IGNORED AND LET IS FOR TRAJECTORY

for python3.6
created on           2020-10-01-10:00:00
full git history thereafter

@author:    Micha (MEmx), IKP TU-Darmstadt, Germany and Univ. Bordeaux, CEA, CNR, CELIA UMR5107, Talence, France

@contact:   michael@michaelmaxx.de

@output:    distances in [um] energy in [MeV]

execute command: python run.py
"""

# =============================================================================
# PYTHON HEADER
# =============================================================================
# EXTERNAL IMPORT
from importlib import reload

import sys
import os

import numpy as np

import time

# Introduce environment for native modules
sys.path.append(os.path.abspath('LIB'))
sys.path.append(os.path.abspath('SCR'))

# PROJECT IMPORT
import functions as func        # !! functional routines applicable to many cases
import stack as stack           # !! functional routines applicable to target stack data
import target as target         # !! functional routines applicable to target layer data
import projectile as projSP     # !! functional routines applicable to projectile data
import bullet as bullet         # !! functional routines applicable to projectiles
import transport as transport   # !! functional routines applicable to projectile transport

reload(func)
reload(stack)

# CONSTANTS

# LOAD COMMAND LINE
# | expect named tuples with the following order:
# |     script      :: script name launched by python
# |     silent      :: optional, default behaviour is TRUE
# |                     if TRUE then NO_USER_DIALOGUE
# |     input_file  :: optional absolute path, default behaviour is NONE
cmd = func.COMMAND_LINE(["script","input_file","silent"])

# ==============================================================================
# SYSTEM RUNTIME ENVIRONMENT
# ==============================================================================
# Take TIME
debut = time.time()

# PROMPT GREETINGS
print('\nWELCOME TO pyPEST\n- Pythonic photon and electron stopping!\n')

# CLEARIFY RUN
# | updates variables:
# |     project_path :: string, pyPEST root directory
# |     work_path    :: string, shell execution directory
project_path,work_path = func.ENVIRONMENT()

# LOAD INPUT FILE
# | load variable dictionary:
# |     species :: string, projectile species
# |     stack   :: string, path to target stack
if cmd.input_file == None:
    input_path = project_path+os.path.sep+'input_file.csv'
elif cmd.input_file.lower() == 'none':
    input_path = project_path+os.path.sep+'input_file.csv'
else:
    input_path = cmd.input_file
    
input_vars = func.READ_CSV(input_path,1,True)

# LOAD TARGET STACK
# | expect lines of either form:
# |     material_thickness          :: material in LIB/dict_targetmaterials.csv and
# |                                     thickness in [um]
# |     material_thickness integer  :: material in LIB/dict_targetmaterials.csv and
# |                                     thickness in [um] with 
# |                                     integer as occurance counter
# |     name integer                :: name in LIB/dict_complextarget.csv and
# |                                     integer as occurance counter
if 'stack' in input_vars.keys():
    stack_path = func.LOOKUP(input_vars['stack']['value'],[project_path, work_path])
else:
    stack_path = project_path+os.path.sep+'stack.txt'
    
target_stack = stack.read(stack_path)
stack.prompt(target_stack)

# LOAD TARGET MATERIALS
if 'dict_targetmaterials' in input_vars.keys():
    targetmaterials_path = func.LOOKUP(input_vars['dict_targetmaterials']['value'],[project_path, work_path])
else:
    targetmaterials_path = project_path+os.path.sep+'LIB'+os.path.sep+'dict_targetmaterials.csv'

materials_dict = target.getMATERIALdict(targetmaterials_path)

# LOAD COMPLEX TARGET LAYERS
if 'dict_complextarget' in input_vars.keys():
    complextarget_path = func.LOOKUP(input_vars['dict_complextarget']['value'],[project_path, work_path])
else:
    complextarget_path = project_path+os.path.sep+'LIB'+os.path.sep+'dict_complextarget.csv'

complex_dict = target.getCOMPLEXdict(complextarget_path)

# PARSE TARGET STACK TO RESPECTIVE LAYERS

target_layers = stack.layers(target_stack,materials_dict,complex_dict)

print(stack.getactivepositions(target_layers))

# CONSTRUCT ORDERED NUMPY ARRAYS ALONG DEPTH AXIS
# | create dictionary with keys 'numpy_min', 'numpy_max' for all layers
# |  in target_layers dict translating sorted depth values into each 'numpy_z'
tl_list_numpy_dict = stack.depthtonumpy(target_layers)

# UPDATE SIMULATION DICTIONARY FOR PROJECTILE STOPPING
if 'dict_speciestostopping' in input_vars.keys():
    speciesstopping_path = func.LOOKUP(input_vars['dict_speciestostopping']['value'],[project_path, work_path])
else:
    speciesstopping_path = project_path+os.path.sep+'LIB'+os.path.sep+'dict_speciestostopping.csv'

if 'projectile' in input_vars.keys():
    projectile = input_vars['projectile']['value']
else:
    print("\nFATAL: PROJECTILE NOT DEFINED")
    func.ERROR_PRINT(1169)

ps_dict = projSP.getSTOPPINGdict(speciesstopping_path,projectile)

# CONSTRUCT ORDERED NUMPY ARRAYS ALONG ENERGY AXIS
# | create dictionary with keys 'numpy' and 'idx_to_key' for all materials
# |  in STOPPINGDICT with sorted energy values in 'numpy'
# |  and [index,key] pairs pointing from the index of the energy in the 'numpy' array
# |  to the energy-related key in the STOPPING DICT
ps_dict_numpy_dict = projSP.energytonumpy(ps_dict)

# CONSTRUCT PROJECTILE SPECTRUM
if 'impactenergy' in input_vars.keys():
    try:
        projectile_energy = float(input_vars['impactenergy']['value'])
    except:
        func.ERROR_PRINT(13)
else:
    print("\nFATAL: PROJECTILE IMPACT ENERGY NOT DEFINED")
    func.ERROR_PRINT(1169)
    
if 'spectrum' in input_vars.keys():
    try:
        projectile_spectrum = input_vars['spectrum']['value']
    except:
        func.ERROR_PRINT(13)
else:
    print("\nWARNING: PROJECTILE IMPACT ENERGY SPECTRUM NOT DEFINED")
    print("> INTERPRET AS MONO-ENERGETIC RUN")
    projectile_spectrum = 'monoenergetic'
    
if projectile_spectrum == 'maxwellian':
    projectile_energy_dist = bullet.maxwellian(projectile_energy,4,100,projectile)
elif projectile_spectrum == 'monoenergetic':
    projectile_energy_dist = np.array([[projectile_energy],[1.]])
else:
    print("\nFATAL: PROJECTILE IMPACT ENERGY SPECTRUM NOT KNOWN")
    func.ERROR_PRINT(1169)

# RUN PROJECTILE ENERGY LOSS
if 'spatialresolution' in input_vars.keys():
    try:
        spatial_res = float(input_vars['spatialresolution']['value'])
    except:
        func.ERROR_PRINT(13)
else:
    print("FATAL: SPATIAL RESOLUTION NOT DEFINED")
    func.ERROR_PRINT(1169)

if projectile != 'photon':
    z_position = [0.]
    z_position,ID,all_dz = transport.spatial_mesh(z_position,tl_list_numpy_dict,spatial_res)
    
    E_LET = np.zeros((len(z_position),))
    
    # .. correct for discrete TODO
    norm = np.sum(projectile_energy_dist[1])
    
    for energy,probability in projectile_energy_dist.T: #np.nditer(projectile_energy_dist[0], op_flags=['readwrite']):
        idz = 0
        while energy > 0. and idz < len(ID):
            # .. push projectiles
            delta_E = transport.push(target_layers[ID[idz]],all_dz[idz],ps_dict_numpy_dict,ps_dict,energy)
            
            # .. update energy
            energy -= delta_E
            
            # .. write out
            E_LET[idz] += delta_E * probability/norm # TODO norm results correction
                          
            # .. update z psoition
            idz += 1
            
            #print('z  = '+str(z_position[-1]))
            #print('dE = '+str(E_LET[-1]))
            #print('E  = '+str(projectile_energy))
        
else:
    print('\nPHOTON ATTENUATION NOT IMPLEMENTED')

# OUTPUT
print('\nRESULTS')
print('RANGE IS Z = '+str(np.nanmax((np.array(z_position))[np.where(E_LET>0.)])))

import matplotlib.pyplot as plt

plt.plot(projectile_energy_dist[0],projectile_energy_dist[1])
plt.title('INPUT')
plt.show()

plt.clf()
 
plt.plot(z_position,E_LET)
plt.title('LET')
plt.show()

# ==============================================================================
# EXIT
# ==============================================================================
# Runtime
fin = time.time()
print("\nTotal runtime = %3.1f s\n" % (fin-debut))

# SAY GOODBYE
func.GOODBYE()