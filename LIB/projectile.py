# -*- coding: utf-8 -*-
"""
This is the stopping library. Please do only add but not delete content.

"""

# =============================================================================
# PYTHON HEADER
# =============================================================================
# External modules for runtime management
from importlib import reload

import sys
import os

# Modules needed in functions
import numpy as np
import math

# Introduce environment for native modules
sys.path.append(os.path.abspath('..'))

# Native modules
import functions as func

# Reload native modules to update them on runtime
reload(func)

# ==============================================================================
# FUNCTIONS TO LOAD STOPPING DATA
# ==============================================================================

def getSTOPPINGdict(path,projectile):
    """
    LOAD PARAMETERS
    PATH            :: LOOKUP PATH
    LOOKUP_TABLE    :: SEARCH FILES HERE
    """
    dict = func.READ_CSV(path,3,True)
    
    try:
        toplayer = dict.keys()
    except:
        func.ERROR_PRINT(232)
    
    if projectile not in toplayer:
        func.ERROR_PRINT(1160)
    
    try:
        parsed_dict = parseSTOPPINGdict(dict[projectile])
        return parsed_dict
    except:
        func.ERROR_PRINT(13)
    
def parseSTOPPINGdict(dict):
    """
    PARSE TO CORRECT TYPES FROM CSV MATERIAL DICT
    DICT :: STOPPING DICT
    """
    parsenames = {'mass_collision_stopping_power_in_MeV_squared_cm_per_g'  : "SP_coll",\
                  'attenuation_coeff'                                      : "AC"}
    for material in dict.keys():
        for energy in dict[material].keys():
            for property in dict[material][energy].keys():
                value = dict[material][energy][property]
                if property in parsenames.keys():
                    key = parsenames[property]
                    if key != property:
                        dict[material][energy].pop(property)
                else:
                    key = property
                try:
                    dict[material][energy][key] = float(value)
                except ValueError:
                    dict[material][energy][key] = value
    return dict

# ==============================================================================
# FUNCTIONS TO ORGANIZE STOPPING DATA
# ==============================================================================
def energytonumpy(dict):
    """
    PARSE ENERGY AXIS TO ORDERED NUMPY ARRAY
    DICT :: STOPPING DICT
    """
    outdict = {}
    for material in dict.keys():
        sorted = []
        for energy in dict[material].keys():
            sorted.append([float(energy),energy])
        sorted.sort()
        
        outdict[material] = {}
        outdict[material]['numpy'] = []
        outdict[material]['index_to_key'] = {}
        
        for idx,item in enumerate(sorted):
            outdict[material]['numpy'].append(item[0])
            outdict[material]['index_to_key'][idx] = item[1]
            
        outdict[material]['numpy'] = np.array(outdict[material]['numpy'])
            
    return outdict
    