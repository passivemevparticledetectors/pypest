# -*- coding: utf-8 -*-
"""
This is the target type library. Please do only add but not delete content.

"""

# =============================================================================
# PYTHON HEADER
# =============================================================================
# External modules for runtime management
from importlib import reload

import sys
import os

# Modules needed in functions
import numpy as np
import math

# Introduce environment for native modules
sys.path.append(os.path.abspath('..'))

# Native modules
import functions as func

# Reload native modules to update them on runtime
reload(func)

# ==============================================================================
# FUNCTIONS TO LOAD TARGET MATERIAL DATA
# ==============================================================================

def getMATERIALdict(path):
    """
    LOAD FITPARAMETERS
    PATH            :: LOOKUP PATH
    LOOKUP_TABLE    :: SEARCH FILES HERE
    """
    dict = func.READ_CSV(path,1,True)
    warnings = ['type_is_FILTER_or_ACTIVE']
    fatals = ['mass_density_in_g_per_cubic_cm']
    if func.validate2Ddict(dict,warnings,fatals):
        parsed_dict = parseMATERIALdict(dict)
        return parsed_dict
    else:
        func.ERROR_PRINT(13)
    
def parseMATERIALdict(dict):
    """
    PARSE TO CORRECT TYPES FROM CSV MATERIAL DICT
    DICT :: MATERIAL DICT
    """
    parsenames = {'mass_density_in_g_per_cubic_cm'  : "density",\
                  'type_is_FILTER_or_ACTIVE'        : "type"}
    for material in dict.keys():
        for property in dict[material].keys():
            value = dict[material][property]
            if property in parsenames.keys():
                key = parsenames[property]
                if key != property:
                    dict[material].pop(property)
            else:
                key = property
            try:
                dict[material][key] = float(value)
            except ValueError:
                dict[material][key] = value
    return dict
    
# ==============================================================================
# FUNCTIONS TO LOAD COMPLEX TARGET DATA
# ==============================================================================

def getCOMPLEXdict(path):
    """
    LOAD FITPARAMETERS
    PATH            :: LOOKUP PATH
    LOOKUP_TABLE    :: SEARCH FILES HERE
    """
    dict = func.READ_CSV(path,1,True)
    warnings = ['array_of_mass_density_in_g_per_cubic_cm_seperator_is_underscore']
    fatals = ['layer_array_of_target_materials_separator_is_underscore',\
              'array_of_width_in_um_seperator_is_underscore',\
              'layers_int_count']
    if func.validate2Ddict(dict,warnings,fatals):
        parsed_dict = parseCOMPLEXdict(dict)
        return parsed_dict
    else:
        func.ERROR_PRINT(13)
    
def parseCOMPLEXdict(dict):
    """
    PARSE TO CORRECT TYPES FROM CSV COMPLEX TARGET DICT
    DICT :: COMPLEX DICT
    """
    parsenames = {'layer_array_of_target_materials_separator_is_underscore'         : "materials",\
                  'array_of_mass_density_in_g_per_cubic_cm_seperator_is_underscore' : "densities",\
                  'array_of_width_in_um_seperator_is_underscore'                    : "widths"}
    outdict = {}
    for name in dict.keys():
        outdict[name] = {}
        for property in dict[name].keys():
            if 'layers_int_count' != property:
                try:
                    value = dict[name][property].split('_')
                except:
                    func.ERROR_PRINT(13)
            else:
                value = [dict[name][property]]
            if property in parsenames.keys():
                key = parsenames[property]
            else:
                key = property
            for i in range(len(value)):
                try:
                    value[i] = float(value[i])
                except ValueError:
                    value[i] = value[i]
            if 'layers_int_count' != property:
                outdict[name][key] = value
            else:
                outdict[name][key] = value[0]
    return outdict
    