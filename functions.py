﻿# -*- coding: utf-8 -*-
"""
This is the function module. Please do only add but not delete content.

"""

# =============================================================================
# PYTHON HEADER
# =============================================================================
# EXTERNAL IMPORT
from importlib import reload

import os
import sys

import numpy as np

from inspect import getsourcefile
from collections import namedtuple as nt

# PROJECT IMPORT
import functions as func  # !! functional routines applicable to many cases

reload(func)

# ==============================================================================
# CONSTANTS USED IN FUNCTIONS
# ==============================================================================

invalid_int = -9999

# ==============================================================================
# FUNCTIONS TO MANAGE RUNTIME ENVIRONMENT
# ==============================================================================
def COMMAND_LINE(expect_list):

    #SEARCH FOR COMMAND LINE INPUT
    arg_names = expect_list
    args = dict(zip(arg_names, sys.argv))

    Arg_list = nt('Arg_list', arg_names)
    args = Arg_list(*(args.get(arg, None) for arg in arg_names))
    
    return args
    
def ENVIRONMENT():
    # TODO CHECK VALIDITY OF THIS FUNCTION
    try:
        scriptpath = os.path.dirname(os.path.abspath(getsourcefile(lambda:0)))
        print("pyPEST root directory    : " + scriptpath)

        workpath = os.path.abspath(os.getcwd())
        print("Shell execution directory: " + workpath)
    except:
        ERROR_PRINT(1287)
    
    return scriptpath,workpath

def HOMEBASE(addr):
    if '~' in addr:
        home = os.path.expanduser("~")
        addr = home + addr.strip('~')
        
    return addr
    
def LOOKUP(path,lookup_table):
    """
    RETURN FIRST EXISTANCE IN ORDER OF TABLE
    PATH            :: LOOKUP PATH
    LOOKUP_TABLE    :: SEARCH FILES HERE
    """
    print('\nSEARCH '+path)

    if path == None:
        ERROR_PRINT(1160)
        
    path = HOMEBASE(path)
    
    if not os.path.isfile(path):
        print(' > DO NOT FIND '+path)
        for lookup in lookup_table:
            if not os.path.isfile(lookup+os.path.sep+path):
                print(' > DO NOT FIND '+lookup+os.path.sep+path)
            else:
                path = lookup+os.path.sep+path
                break
    
    print('FIND   '+path)
    return path
    
def STRIP_EXTENSION(filename):
    root = ((filename[::-1]).split('.',1)[1])[::-1]
    return root
    
def GIVEINVALIDINT():
    
    return invalid_int
    
def CHECKINT(integer):

    if integer == invalid_int:
        check = False
    else:
        check = True
    
    return check
    
def POST_LTR(arraylike,destination):
    # UNDERSTAND DATA STRUCTURE #TODO CURRENTLY MAX 3 LEVELS
    arraylike = np.array(arraylike)
    structure = arraylike.shape
    # WRITE OUT
    with open(destination, 'w') as f:
        for yitem in arraylike:
            for xitem in yitem:
                if isinstance(xitem,list):
                    xitem = '['+(" ".join(xitem))+']'
                f.write("%s " % xitem)
            f.write("\n")
            
def READ_LTR(path):
    with open(path, 'r') as f:
        letter = f.readlines()
    arraylike = []
    for line in range(len(letter)):
        letter[line] = letter[line].strip()
        if "[" in letter[line]:
            letter[line] = letter[line].strip('[]')
            letter[line] = letter[line].split('] [') #TODO , >
            if len(letter[line]) != 0:
                for element in range(len(letter[line])):
                    letter[line][element] = letter[line][element].split()
        else:
            letter[line] = letter[line].split()
        arraylike.append(letter[line])
    return np.array(arraylike)

def READ_CSV(path,dictcols=1, header = False): # TODO ADD ARGUMENT TO SPECIFY VAR DATA-TYPE (FLOAT,STR,MIXED) FOR PARSER (*)
    """
    READ CSV FILE TO DICT
    PATH               :: LOOKUP PATH
    DICTCOLS, OPTIONAL :: ADD FIRST N COLS TO DICTKEYS DICT{COL_1},..,{COL_N},{(ROW_N+1:) :: VALUE}
    HEADER, OPTIONAL   :: PRESENCE OF HEADER
    """
    if header:
        try:
            #print(path)
            with open(path,'r') as openfile:
                csv_dict,row_dict = {},{}
                head = openfile.readline().replace('\n', '').split(',')[dictcols:]
                for col in head:
                    row_dict[col] = head.index(col) + dictcols
                for row in openfile :
                    new_dict = {}
                    # Retrive Data
                    row_array = row.strip().split(',')
                    data_dict = {}
                    for col in head:
                        try:
                            data_dict[col] = float(row_array[row_dict[col]]) # (*)
                        except:
                            data_dict[col] = row_array[row_dict[col]] # (*)
                    new_dict = data_dict.copy()
                    # Retrive Keys
                    row_keys = row_array[:dictcols]
                    for key in reversed(row_keys):
                        new_dict = {key : new_dict.copy()}
                    # UPDATE DICT
                    csv_dict = MERGE_DICT(csv_dict,new_dict)
                    #csv_dict.update(new_dict)
   
                return csv_dict
        except:
            ERROR_PRINT(2)
    else:
        ERROR_PRINT(13)

def MERGE_DICT(dict1, dict2):
    dict3 = {**dict1, **dict2}
    for key, value in dict3.items():
        if key in dict1 and key in dict2:
            if isinstance(dict1[key],dict) and isinstance(dict2[key],dict):
                # DICT TO DICT MERGE
                dict3[key] = MERGE_DICT(dict1[key],dict2[key])
            else:
                # VALUE TO ARRAY MERGE
                dict3[key] = [value , dict1[key]]
    return dict3
    
def validate2Ddict(dict,warnings,fatals):
    """
    CHECK PERSITANCE OF COMPLEX TARGET DICT
    DICT     :: 2D DICT {1D : {2D : values}}
    WARNINGS :: RAISE WARNINGS FOR MISSING IN 2D
    FATALS   :: RAISE FATALS FOR MISSING IN 2D
    """
    try:
        toplayer = dict.keys()
    except:
        ERROR_PRINT(232)
    
    validate = True
    
    for entry in toplayer:
        try:
            properties = dict[entry].keys()
        except:
            ERROR_PRINT(1160)
        for fatal in fatals:
            if fatal not in properties:
                print('\nPROBABLY FATAL: NOT SPECIFIED')
                print(' > '+fatal+' for '+entry)
                validate = False
        for warning in warnings:
            if warning not in properties:
                print('\nWARNING: NOT SPECIFIED')
                print(' > '+warning+' for '+entry)
    
    return validate

def ERROR_PRINT(eid):
    # ID   :: LABEL
    #    0 :: ERROR_DIVISION_BY_ZERO
    #         = The system cannot divide by zero.
    #    2 :: ERROR_FILE_NOT_FOUND
    #         = The system cannot find the file specified.
    #    5 :: ERROR_ACCESS_DENIED
    #         = Access is denied.
    #   13 :: ERROR_INVALID_DATA
    #         = The data is invalid.
    #  161 :: ERROR_BAD_PATHNAME
    #         = The specified path is invalid.
    #  232 :: ERROR_NO_DATA
    #         = The pipe is being closed.
    #  677 :: ERROR_EXTRANEOUS_INFORMATION
    #         = Too Much Information.
    # 1160 :: ERROR_SOURCE_ELEMENT_EMPTY        
    #         = The indicated source element has no media.
    # 1169 :: ERROR_NO_MATCH    
    #         = There was no match for the specified key in the index.
    # 1287 :: ERROR_UNIDENTIFIED_ERROR
    #         = Insufficient information exists to identify the cause of failure.
    # 8322 :: ERROR_DS_RANGE_CONSTRAINT
    #         = A value for the attribute was not in the acceptable range of values.

    if eid == 0:
        print('\nERROR_DIVISION_BY_ZERO\nThe system cannot divide by zero.\n')
    elif eid == 2:
        print('\nERROR_FILE_NOT_FOUND\nThe system cannot find the file specified.\n')
    elif eid == 5:
        print('\nERROR_ACCESS_DENIED\nAccess is denied.\n')
    elif eid == 13:
        print('\nERROR_INVALID_DATA\nThe data is invalid.\n')
    elif eid == 161:    
        print('\nERROR_BAD_PATHNAME\nThe specified path is invalid.\n')
    elif eid == 232:
        print('\nERROR_NO_DATA\nThe pipe is being closed.\n')
    elif eid == 677:
        print('\nERROR_EXTRANEOUS_INFORMATION\nToo Much Information.\n')
    elif eid == 1160:
        print('\nERROR_SOURCE_ELEMENT_EMPTY\nThe indicated source element has no media.\n')
    elif eid == 1169:
        print('\nERROR_NO_MATCH\nThere was no match for the specified key in the index.\n')
    elif eid == 1287:
        print('\nERROR_UNIDENTIFIED_ERROR\nInsufficient information exists to identify the cause of failure.\n')
    elif eid == 8322:
        print('\nERROR_DS_RANGE_CONSTRAINT\nA value for the attribute was not in the acceptable range of values.\n')
    else:
        print('\nERROR\nNo idea what happened.\n')
        
    EXIT()

def GOODBYE():

    print("exitcute")

def EXIT():

    print('\nEXIT() BEFORE END')
    GOODBYE()
    raise SystemExit(0) # this always raises SystemExit
    print("Something went horribly wrong")
