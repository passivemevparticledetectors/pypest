﻿# -*- coding: utf-8 -*-
"""
This is the projectile transport handler. Please do only add but not delete content.

"""

# =============================================================================
# PYTHON HEADER
# =============================================================================
# External modules for runtime management
from importlib import reload

import sys
import os

# Modules needed in functions
import numpy as np

# Introduce environment for native modules
sys.path.append(os.path.abspath('..'))
sys.path.append(os.path.abspath('../LIB'))
sys.path.append(os.path.abspath('LIB'))

# CONSTANTS
cm_to_um = 10000.

# Native modules
import functions as func

# Reload native modules to update them on runtime
reload(func)

# ==============================================================================
# FUNCTIONS TO PUSH PARTICLES
# ==============================================================================

def push(host_layer,dz,ps_dict_numpy_dict,ps_dict,projectile_energy):
    """
    Handle projectile push.
    
    """
    # Allocate variables
    
    # Evaluate fields
    
    # Particle pusher
    
    # Determine energy loss
    # .. find hosting layer
    host = host_layer

    # .. find nearest energy position
    idE = (np.abs(ps_dict_numpy_dict[host['name']]['numpy'] - projectile_energy)).argmin()
    
    # .. check interpolation method and need of extrapolation
    method = 'linear'
    
    if method != 'nearest':
        if projectile_energy < ps_dict_numpy_dict[host['name']]['numpy'][idE] and idE != 0:
            idE = idE - 1
    
    extrapolate = False
    fall_out = 'none'
    
    if method != 'nearest':
        check_up = (projectile_energy >= np.nanmax(ps_dict_numpy_dict[host['name']]['numpy']))
        check_lo = (projectile_energy < np.nanmin(ps_dict_numpy_dict[host['name']]['numpy']))
        if check_lo or check_up:
            extrapolate = True
            if check_lo:
                fall_out = 'low'
            else:
                fall_out = 'up'
    
    keyE = ps_dict_numpy_dict[host['name']]['index_to_key'][idE]
    
    # .. refine energy position
    if extrapolate == False or fall_out == 'low':
        lower_keyE = keyE
        upper_keyE = ps_dict_numpy_dict[host['name']]['index_to_key'][idE+1]
    else:
        if fall_out == 'up':
            lower_keyE = ps_dict_numpy_dict[host['name']]['index_to_key'][idE-1]
            upper_keyE = keyE
        # for fall out down, the idE was not changed herebefore
    #print('kEl= '+str(lower_keyE))
    #print('kEu= '+str(upper_keyE))
    
    # .. calculate stopping power value
    SP = 0.
    if method == 'nearest':
        for meca in ps_dict[host['name']][upper_keyE].keys():
            SP += ps_dict[host['name']][keyE][meca]
    elif method == 'linear':
        # interpolate
        DSP = 0.
        base = 0.
        for meca in ps_dict[host['name']][upper_keyE].keys():
            #print(meca+': UP = '+str(ps_dict[host['name']][upper_keyE][meca]))
            #print(meca+': LO = '+str(ps_dict[host['name']][lower_keyE][meca]))
            DSP += ps_dict[host['name']][upper_keyE][meca] - ps_dict[host['name']][lower_keyE][meca]
            base += ps_dict[host['name']][lower_keyE][meca]
        
        DE = float(upper_keyE)-float(lower_keyE)
        #print('DE = '+str(DE))
        SP = base + DSP/DE * (projectile_energy - float(lower_keyE))
        
    #elif method == 'log':
    
    else:
        func.ERROR_PRINT(13)

    # .. determine energy loss
    delta_E = SP * host_layer['density'] / cm_to_um * dz
    
    # .. zero energy
    if delta_E > projectile_energy:
        delta_E = projectile_energy
    
    return delta_E
    
def spatial_mesh(z_position,tl_list_numpy_dict,spatial_res):
    """
    Z_POSITION :: DEFINED ARRAY OF POSITIONS THAT IS PROPAGATED THROUGH THE TARGET
    
    """
    all_dz = []
    ID = []

    while z_position[-1] <= np.nanmax(tl_list_numpy_dict['numpy_max']):
        # .. find positions and ID's
        idl = (np.abs(tl_list_numpy_dict['numpy_max'] - z_position[-1])).argmin()
        if z_position[-1] >= tl_list_numpy_dict['numpy_max'][idl]:
            idl = idl + 1
        if z_position[-1] >= np.nanmax(tl_list_numpy_dict['numpy_max']) or z_position[-1] < np.nanmin(tl_list_numpy_dict['numpy_min']):
            idl = None
        
        if idl != None:
            ID.append(idl)
        else:
            break
        # .. find new propagation length
        remain_in_host = tl_list_numpy_dict['numpy_max'][idl] - z_position[-1]
        if remain_in_host > spatial_res:
            dz = spatial_res
        else:
            dz = remain_in_host
        all_dz.append(dz)
        # .. do mesh
        z_position.append(z_position[-1] + dz)
    
    return z_position,ID,all_dz

def inter_extra_polate(searchfor,meshpoints,meshedvals):
    """
    Handle values between nodes on a mesh
    SEARCHFOR   :: SPATIAL/TEMPORAL POINS IN NUMPY ARRAY
    MESHPOINTS  :: SPATIAL/TEMPORAL AXIS
    MESHEDVALS  :: MESHED VALUES
    METHOD      :: METHOD
    """
    for point in np.nditer(searchfor):
        # ..find nearest gridpoint
        id_closest = (np.abs(meshpoints - point)).argmin()
        
        # ..find if we are inside our otside mesh
        interextrapolate = None
        if point < np.nanmin(meshpoints):
            interextrapolate = 'lower'
        elif point > np.nanmax(meshpoints):
            interextrapolate = 'upper'
        
        # ..
    
    if method == 'nearest':        
        do = nothing
    elif method == 'linear':
        do = nothing
    elif method == 'log':
        do = nothing
    else:
        do = nothing
    return do
    
        
               

# ==============================================================================
# FUNCTIONS TO HANDLE ENERGY LOSS
# ==============================================================================
    