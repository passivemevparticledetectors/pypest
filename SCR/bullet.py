﻿# -*- coding: utf-8 -*-
"""
This is the projectile handler. Please do only add but not delete content.

"""

# =============================================================================
# PYTHON HEADER
# =============================================================================
# External modules for runtime management
from importlib import reload

import sys
import os

# Modules needed in functions
import numpy as np

# Introduce environment for native modules
sys.path.append(os.path.abspath('..'))
sys.path.append(os.path.abspath('../LIB'))
sys.path.append(os.path.abspath('LIB'))

# CONSTANTS
atomic_mass_unit = 1.66053906660e-27
electron_mass = 9.10938188e-31

electron_charge = 1.60217646e-19

speed_of_light = 299792458.

MeV_to_J = electron_charge * 1.e6

# Native modules
import functions as func

# Reload native modules to update them on runtime
reload(func)

# ==============================================================================
# FUNCTIONS TO PUSH PARTICLES
# ==============================================================================

def maxwellian(temperature_MeV,span,raster,species):
    """
    GIVES A NORMALIZED MAXWELLIAN DISTRIBUTION [energies][probability]
    TEMPERATURE_MEV :: TEMPERATURE FOR DISTRIBUTION
    SPAN            :: WIDTH IN INTEGER NUMBER OF WIDTHS OF MODUS
    RASTER          :: GRIDPOINTS IN ONE MODUS WIDTH
    """
    def f_maxbol(velocity,mass,temperature):
        a = np.sqrt(temperature/mass)
        return np.sqrt(2./np.pi) * velocity**2/a**3 * np.exp(-velocity**2/(2.*a**2)) 
    
    def ener_to_vel(energy,mass):
        return np.sqrt(2.*energy/mass)
        
    def vel_to_ener(velocity,mass):
        return 1./2.*mass*velocity**2
    
    if species == 'electron':
        mass = electron_mass
    else:
        func.ERROR_PRINT(1160)
    
    modus = ener_to_vel(temperature_MeV*MeV_to_J,mass)
    v_bin = modus/raster
    v_max = max([modus * span,speed_of_light])
    
    velocities = np.arange(0.,v_max,v_bin)
    probabilities = f_maxbol(velocities,mass,temperature_MeV*MeV_to_J)
    
    energies = vel_to_ener(velocities,mass)*(MeV_to_J)**(-1)
    
    return np.array([energies,probabilities])
    
    