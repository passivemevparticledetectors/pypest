﻿# -*- coding: utf-8 -*-
"""
This is the stack data handler. Please do only add but not delete content.

"""

# =============================================================================
# PYTHON HEADER
# =============================================================================
# External modules for runtime management
from importlib import reload

import sys
import os

# Modules needed in functions
import numpy as np

# Introduce environment for native modules
sys.path.append(os.path.abspath('..'))
sys.path.append(os.path.abspath('../LIB'))
sys.path.append(os.path.abspath('LIB'))

# Native modules
import functions as func

# Reload native modules to update them on runtime
reload(func)

# ==============================================================================
# FUNCTIONS TO TREAT STACK
# ==============================================================================

def read(path):
    """
    READ STACK FILE AND CREATE ARRAY [name,occurances,type]
    PATH :: STACK FILE ORIGIN
    """
    # DEFINE SHELL
    data = []
    # HANDLE INPUT
    try:
        with open(path) as inputfile:
            for line in inputfile:
                notsimple = True
                try: # TEST VALIDITY
                    shred = line.split()
                except:
                    try:
                        shred = line.split('_')
                        notsimple = False
                    except:
                        func.ERROR_PRINT(13)
                # IGNORE ADDITIONAL ENTRIES AS IF COMMENTS
                if not notsimple: 
                    data.append({'name' : line,'count' : '1','type' : 'TARGET'})
                elif '_' in shred[0]:
                    data.append({'name' : shred[0],'count' : shred[1],'type' : 'TARGET'})
                else:
                    data.append({'name' : shred[0],'count' : shred[1],'type' : 'COMPLEX'})
    except:
        func.ERROR_PRINT(2)
    # GIVE OUTPUT
    return data
    
def prompt(data):
    """
    PRINT STACK INFORMATION PROPERLY TO TERMINAL
    DATA :: STACK DATA ARRAY
    """
    # TEST VALIDITY
    try:
        print('\nCURRENT STACK:')
        for dataset in data:
            try:
                layer = dataset['name']
                count  = dataset['count']
                type  = dataset['type']
            except:
                func.ERROR_PRINT(13)      
            print(type+'\t '+layer+" times "+count)
        print('END OF STACK')
    except:
        func.ERROR_PRINT(1160)
    # NO OUTPUT VAR
    
def layers(data,materials,complex):
    """
    CREATE ARRAY WITH LAYERS
    DATA      :: STACK DATA ARRAY
    MATERIALS :: MATERIAL DATA DICTIONARY
    COMPLEX   :: COMPLEX TARGET DICTIONARY
    """
    # TEST DATA VALIDITY
    unstacked = []
    try:
        for dataset in data:
            try:
                name = dataset['name']
                count  = dataset['count']
                type  = dataset['type']
            except:
                func.ERROR_PRINT(13)
            #EXPAND DATA
            for i in range(int(count)):
                unstacked.append({'name' : name,'type' : type})
    except:
        func.ERROR_PRINT(1160)
    # INFORMATION TO LAYER
    layers = []
    for layer in unstacked:
        try:
            if "TARGET" == layer['type']:
                new_layer = {}
                shred = layer['name'].split("_")
                new_layer['name'] = shred[0]
                new_layer['width'] = float(shred[1])
                for key in materials[new_layer['name']].keys():
                    new_layer[key] = materials[new_layer['name']][key]
                if len(layers) == 0 :
                    dz = 0.
                else:
                    dz = layers[-1]['z_max']
                new_layer['z_min'] = 0. + dz
                new_layer['z_ctr'] = 0. + dz + new_layer['width']/2.
                new_layer['z_max'] = 0. + dz + new_layer['width']
                layers.append(new_layer)
            elif "COMPLEX" == layer['type']:
                for id in range(int(complex[layer['name']]['layers_int_count'])):
                    new_layer = {}
                    new_layer['name'] = complex[layer['name']]['materials'][id]
                    new_layer['width'] = complex[layer['name']]['widths'][id]
                    for key in materials[new_layer['name']].keys():
                        new_layer[key] = materials[new_layer['name']][key]
                    new_layer['density'] = complex[layer['name']]['densities'][id]
                    if len(layers) == 0 :
                        dz = 0.
                    else:
                        dz = layers[-1]['z_max']
                    new_layer['z_min'] = 0. + dz
                    new_layer['z_ctr'] = 0. + dz + new_layer['width']/2.
                    new_layer['z_max'] = 0. + dz + new_layer['width']
                    layers.append(new_layer)
        except:
            func.ERROR_PRINT(1287)
    return layers
    
def getactivepositions(data):
    """
    PRINT ACTIVE LAYER INFORMATION PROPERLY TO ARRAY
    DATA :: LAYER DATA ARRAY
    """
    # SCAN ACTIVE LAYERS
    print('\nFIND ACTIVE LAYER BONDS (min,max,center)')
    # ASSEMBLE ARRAY
    STAT = []
    dtype = [('startpos', "float"),('endpos', "float"),('centerpos', "float")]
    # TEST VALIDITY
    try:
        for layer in data:
            if layer['type'] == 'ACTIVE':
                tup = layer['z_min'], layer['z_max'], layer['z_ctr']
                STAT.append(tup)
    except:
        func.ERROR_PRINT(1160)
    return np.array(STAT, dtype=dtype)

# ==============================================================================
# FUNCTIONS TO ORGANIZE STACK DATA
# ==============================================================================
def depthtonumpy(inputlist):
    """
    PARSE DEPTH AXIS TO ORDERED NUMPY ARRAY
    INPUTLIST :: ORDERED TARGET LAYER LIST
    """
    outdict = {}
    outdict['numpy_min'] = []
    outdict['numpy_max'] = []
    
    for idx,item in enumerate(inputlist):
        outdict['numpy_max'].append(item['z_max'])
        outdict['numpy_min'].append(item['z_min'])
    
    outdict['numpy_max'] = np.array(outdict['numpy_max'])
    outdict['numpy_min'] = np.array(outdict['numpy_min'])
    
    return outdict
    
    
    